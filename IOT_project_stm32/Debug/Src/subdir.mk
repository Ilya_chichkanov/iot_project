################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/IMU_sens.c \
../Src/MadgwickAHRS.c \
../Src/Madqzh.c \
../Src/lsm9ds1_reg.c \
../Src/main.c \
../Src/stm32l4xx_hal_msp.c \
../Src/stm32l4xx_it.c \
../Src/syscalls.c \
../Src/system_stm32l4xx.c 

OBJS += \
./Src/IMU_sens.o \
./Src/MadgwickAHRS.o \
./Src/Madqzh.o \
./Src/lsm9ds1_reg.o \
./Src/main.o \
./Src/stm32l4xx_hal_msp.o \
./Src/stm32l4xx_it.o \
./Src/syscalls.o \
./Src/system_stm32l4xx.o 

C_DEPS += \
./Src/IMU_sens.d \
./Src/MadgwickAHRS.d \
./Src/Madqzh.d \
./Src/lsm9ds1_reg.d \
./Src/main.d \
./Src/stm32l4xx_hal_msp.d \
./Src/stm32l4xx_it.d \
./Src/syscalls.d \
./Src/system_stm32l4xx.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -DUSE_HAL_DRIVER -DSTM32L496xx -I"C:/IOT_project/IOT_project/Inc" -I"C:/IOT_project/IOT_project/Drivers/STM32L4xx_HAL_Driver/Inc" -I"C:/IOT_project/IOT_project/Drivers/STM32L4xx_HAL_Driver/Inc/Legacy" -I"C:/IOT_project/IOT_project/Drivers/CMSIS/Device/ST/STM32L4xx/Include" -I"C:/IOT_project/IOT_project/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


