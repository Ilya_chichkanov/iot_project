import grafica.*;
import processing.serial.*;
 
Serial myPort;
String inBuffer;
 
String[] token;
int counter = 0;
float pitch, yaw, roll = 0;
float TO_ANG = 180.0/PI;
float TO_RAD = PI/180;

GPlot plotPitch;
GPlot plotYaw;
GPlot plotRoll;
 
void setup()
{
  size(1400, 750, P3D);
 
  plotPitch = new GPlot(this);
  plotPitch.setPos(300,0);
  plotPitch.setDim(1000, 150);
  // Set the plot title and the axis labels
  plotPitch.setTitleText("Pitch");
  plotPitch.setYLim(-180, 180);
  plotPitch.getXAxis().setAxisLabelText("Number of points");
  plotPitch.getYAxis().setAxisLabelText("Value of pitch");
 
  plotYaw = new GPlot(this);
  plotYaw.setPos(300,250);
  plotYaw.setDim(1000, 150);
  // Set the plot title and the axis labels
  plotYaw.setTitleText("Yaw");
  plotYaw.setYLim(-180, 180);
  plotYaw.getXAxis().setAxisLabelText("Number of points");
  plotYaw.getYAxis().setAxisLabelText("Value of yaw");
 
  plotRoll = new GPlot(this);
  plotRoll.setPos(300,500);
  plotRoll.setDim(1000, 150);
  // Set the plot title and the axis labels
  plotRoll.setTitleText("Roll");
  plotRoll.setYLim(-180, 180);
  plotRoll.getXAxis().setAxisLabelText("Number of points");
  plotRoll.getYAxis().setAxisLabelText("Value of roll");
  printArray(Serial.list());
  myPort = new Serial(this, "COM7", 115200);
  myPort.bufferUntil('\n');
  println("START");
  println("START"); 
}
 
void draw()
{
 
  if(counter>700){
    plotPitch.removePoint(0);
    plotYaw.removePoint(0);
    plotRoll.removePoint(0);
  }
 
  plotPitch.addPoint(counter, pitch);
  plotYaw.addPoint(counter, yaw);
  plotRoll.addPoint(counter, roll);  
  counter+=1;
 
  background (0);
  lights();
  
  DrawFigure(pitch,yaw, roll+180.0);
  
  plotPitch.beginDraw();
  plotPitch.drawBackground();
  plotPitch.drawBox();
  plotPitch.drawXAxis();
  plotPitch.drawYAxis();
  plotPitch.drawTitle();
  plotPitch.drawGridLines(GPlot.BOTH);  
  plotPitch.drawLines();
  plotPitch.drawPoints();
  plotPitch.endDraw();
 
  plotYaw.beginDraw();
  plotYaw.drawBackground();
  plotYaw.drawBox();
  plotYaw.drawXAxis();
  plotYaw.drawYAxis();
  plotYaw.drawTitle();
  plotYaw.drawGridLines(GPlot.BOTH);    
  plotYaw.drawLines();
  plotYaw.drawPoints();
  plotYaw.endDraw();
 
  plotRoll.beginDraw();
  plotRoll.drawBackground();
  plotRoll.drawBox();
  plotRoll.drawXAxis();
  plotRoll.drawYAxis();
  plotRoll.drawTitle();
  plotRoll.drawGridLines(GPlot.BOTH);  
  plotRoll.drawLines();
  plotRoll.drawPoints();
  plotRoll.endDraw();
}
 
void serialEvent(Serial myPort)
{
  // read the serial buffer:
  inBuffer = myPort.readStringUntil('\n');
  myPort.clear();
  // if we get any bytes other than the linefeed:
  if (inBuffer != null)
  {
    try{
        token = splitTokens(inBuffer);
        roll = TO_ANG*float(inBuffer.split(" ")[0]);
        pitch = TO_ANG*float(inBuffer.split(" ")[1]);
        yaw = TO_ANG*float(inBuffer.split(" ")[2]);        
        println(roll + " " + pitch + " " + yaw);  
        //print(" ");
        //print(yaw);
        //print(" ");
        //println(roll);
       }catch(Exception e){
            println("Error parsing:");
       }      
  }
  else{
    println("fail");
  }  
}

void DrawFigure(float roll, float pitch, float yaw) {
  //Draw system of coordinate
  pushMatrix();
  translate(150, 400, 0);
  stroke (0, 255, 0);//Z - green (X)
  line(0, 0, 0, 0, 0, 130 );
  stroke (0, 0, 255);// Y - blue (Z)
  line(0, 0, 0, 0, -130, 0 );
  stroke (255, 0, 0);// X - red (Y)
  line(0, 0, 0, 130, 0, 0 ); 
  popMatrix();  
  
  //Draw cube
  pushMatrix();
  translate(150, 400, 0);
  rotateZ(-pitch*TO_RAD);
  rotateY(yaw*TO_RAD);
  rotateX(-roll*TO_RAD); 
  stroke (255, 255, 255);
  box(50);
  popMatrix();  
  
  //Draw system of coordinate of cube
  pushMatrix();
  translate(150, 400, 0);
  rotateZ(-pitch*TO_RAD);
  rotateY(yaw*TO_RAD);
  rotateX(-roll*TO_RAD); 
  stroke (0, 255, 0);//Z - green (X)
  line(0, 0, 0, 0, 0, 130 );
  stroke (0, 0, 255);// Y - blue (Z)
  line(0, 0, 0, 0, -130, 0 );
  stroke (255, 0, 0);// X - red (Y)
  line(0, 0, 0, 130, 0, 0 );
  popMatrix();
}
